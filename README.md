# koa-swagger

**1、环境搭建**

-   安装最新版nodejs
-   新建文件夹koa-swagger，在文件夹内创建swagger-ui与swagger-editor文件夹，创建package.json、app.js文件

**2、下载swagger-ui**

      打开[swagger-ui](https://github.com/swagger-api/swagger-ui) github主页，下载zip压缩包，解压后把dist目录下的所有文件拷贝到_/koa-swagger/swagger-ui文件夹下面_

**3、下载swagger-editor**

打开[swagger-editor](https://github.com/swagger-api/swagger-editor) github主页，下载zip压缩包，解压后把dist目录下的所有文件拷贝到_/koa-swagger/swagger-editor件夹下面_

**4、编辑package.json文件**

```json
{
  "name": "koa-swagger",
  "scripts": {
    "start": "pm2 start app.js --name=swagger"
  },
  "dependencies": {
    "koa": "^2.0.0",
    "koa-bodyparser": "^3.2.0",
    "koa-compress": "^2.0.0",
    "koa-convert": "^1.2.0",
    "koa-multer": "^1.0.2",
    "koa-router": "^7.0.1",
    "koa-static": "^4.0.2",
    "koa-static-cache": "^5.1.1",
    "koa2-cors": "^2.0.5"
  }
}
```

**5、编辑app.js文件**

```javascript
const Koa = require('koa')
const app = new Koa()
const path = require('path')
const bodyParser = require('koa-bodyparser')
const convert = require('koa-convert')
const staticCache = require('koa-static-cache')
const compress = require('koa-compress')
const cors = require('koa2-cors')

// 解析请求体
app.use(bodyParser())

// 跨域设置
app.use(convert(cors({
	allowMethods: ['GET', 'POST'],
    allowHeaders: ['Content-Type', 'Accept'],
    origin: function(ctx) {
        return '*' // 本地环境
    }
})))

// 中间件 设置gzip
app.use(compress({
    threshold: 2048,
    flush: require("zlib").Z_SYNC_FLUSH
}))

// 静态文件服务，把koa-swagger作为根目录
app.use(convert(staticCache(path.join(__dirname, './'), {
    maxAge: 365 * 24 * 60 * 60,
    dynamic: false // 是否动态监测文件变化
})))

// 服务端口
const port = 8882

// 启动程序，监听端口
app.listen(port, () => {
	console.log(`listening on port ${port} for swagger`)
})
```

**6、安装依赖**

      进入koa-swagger文件夹，命令行输入npm指令

```
npm install -g pm2
```

```
npm install
```

**7、启动服务**

进入koa-swagger文件夹，命令行输入npm指令

```
npm run start
```

命令行窗口出现如下效果表示成功

![](https://oscimg.oschina.net/oscnet/0e6313f63d1b696243d2c9093a607c5a55a.jpg)

**8、预览效果**

打开浏览器输入：http://localhost:8882/swagger-ui/index.html和http://localhost:8882/swagger-editor/index.html查看

**9、指定swagger.json文件**

打开_/koa-swagger/swagger-ui/index.html_，编辑Build a system部分

```javascript
// Build a system
const ui = SwaggerUIBundle({
    url: "http://localhost:8882/swagger.json", // 修改部分，改为自己的json配置文件，用swagger-editor编辑并导出来
    dom_id: '#swagger-ui',
    deepLinking: true,
    presets: [
        SwaggerUIBundle.presets.apis,
        SwaggerUIStandalonePreset
    ],
    plugins: [
        SwaggerUIBundle.plugins.DownloadUrl
    ],
    layout: "StandaloneLayout"
})
```

     把swagger-editor编辑并导出的swagger.json文件拷贝到_/koa-swagger目录，我的_swagger.json文件内容如下

```
swagger: "2.0"
info:
  version: "0.0.1"
  title: "react-talkweb"
host: localhost:8881
tags:
- name: "test"
  description: "测试接口"
paths:
  /test/login:
    post:
      tags:
      - "test"
      summary: "登录接口"
      parameters:
      - name: "param"
        in: "body"
        description: '登录接口参数'
        required: true
        schema:
          $ref: '#/definitions/loginParam'
      responses:
        200:
          description: "Success"

  /test/portal:
    post:
      tags:
      - "test"
      summary: "选择系统接口"
      parameters:
      - name: "param"
        in: "body"
        description: "选择系统接口参数"
        required: true
        schema:
          $ref: '#/definitions/portalParam'
      responses:
        200:
          description: "Success"
  
  /test/home:
    post:
      tags:
      - "test"
      summary: "首页接口"
      parameters:
      - name: "param"
        in: "body"
        description: "首页接口参数"
        required: true
        schema:
          $ref: '#/definitions/homeParam'
      responses:
        200:
          description: Success

definitions:
  loginParam:
    properties:
      username:
        type: "string"
        default: "admin"
      password:
        type: "string"
        default: "test@123"
    required:
      - "username"
      - "password"
  portalParam:
    properties:
      id:
        type: "string"
    required: 
      - "id"
  homeParam:
    properties:
      userid:
        type: "string"
      systemid:
        type: "string"
    required: 
      - "userid"
      - "systemid"
```

**10、重启pm2服务**

```
pm2 restart all
```